FROM python:3.6

RUN mkdir -p /leaderboard/src/frontend/build
WORKDIR /leaderboard

RUN pip install gunicorn==19.9.0
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY src/*.py ./src/
COPY src/frontend/build ./src/frontend/build

CMD [ "sh", "-c", "exec python -m gunicorn.app.wsgiapp -w $WORKERS -b 0.0.0.0:$PORT --log-level DEBUG \"src.server:the_app()\"" ]
