# Datahack Leaderboard
This is the code for [Datahack](www.datahack.org.il)'s leaderboard server.

It's designed to provide a Kaggle-like experience, with the following assumptions:

1. The hackathon can contain multiple independent challenges running in parallel.
1. Each challenge has a sponsor (the company).
1. While the challenge is ongoing, users can submit their predictions for the test set. 

## Instructions for challenge sponsors
Look at [src/sponsor_server.py](./src/sponsor_server.py) for the simplest possible implementation of a sponsor server.
The contract is described in this sequence diagram:

![Sequence diagram of user submitting predictions to leaderboard server and getting the scores from the sponsor server](./docs/sponsor-server-seq-diagram.png)


The sponsor server is expected to receive HTTP POST requests, with `application/json` content type.   
The content itself can be any kind of json entity (object, array, string, number, whatever).   
The expected return type is another json object of the form:    
`{public_score: 3.14, secret_score: 1.618}`

For more explanation on the submission API and the sponsor server, see:

* [scripts/submit.py](./scripts/submit.py) for the code the user will need to run to submit to the leaderboard server.
* [src/sponsor_server.py](./src/sponsor_server.py) for a dummy example of an implementation of the sponsor server.

The leaderboard server will display the public scores, until the competition is finished.
Once it's finished, further submissions from users will be rejected, and the secret scores will be displayed instead.  

## Instructions for maintainers / users
The project is mostly built on [Flask](https://palletsprojects.com/p/flask/) and https://github.com/agoragames/python-leaderboard   

The frontend is a very basic React app, found in `src/frontend`.   
Before running a dev server (most easily done using [run-dev.sh](./run-dev.sh)), you should `yarn build` the frontend at 
least once to create the `src/frontend/build` folder, from which the Flask server serves static content.

For convenience, you can use `yarn watch` to continually recompile the frontend while you're developing.

In production, the server is intended to be packed as a docker container.   
It requires a working Redis instance to connect to, otherwise it will crash on start.   
All configuration for the docker container is available through environment variables.   

See [Dockerfile](./Dockerfile) and [src/server.py](./src/server.py) for the available env variables, but the important ones are:

* Redis connection info
* Port to listen on
* Whether to check user passwords on submission. 
  This will require each participant in the competition to register with the leaderboard server
  administrator before they can participate!

[gunicorn](https://gunicorn.org/) is used to run the server in production instead of Flask.

### Data model

All data is stored in Redis. [src/conf.py](./src/conf.py) contains the DAL.

Leaderboards have to be defined by calling [src/conf.py](./src/conf.py) with the `--add` flag prior to the competition's start.   
The most important part here is the leaderboard's name and sponsor server URL.   
The name determines the URL that the leaderboard will be served on.   
E.g. a leaderboard named R2 will be served at http://leaderboard-server.com/R2

**New leaderboards (or removal of leaderboards) only takes effect after server restart!**   
Same goes for changing environment variables, including the choice to use user passwords.

An important caveat is that at the time of writing this, the Datahack 2019 sponsors' logos are hard coded in specific paths 
in the `src/fronted/src/assets/` folder. 

You can create leaderboards with different names, but their logos will be missing.

This can be relatively easily fixed by storing the sponsor's logo URL as part of the leaderboard definition. 

### Possible improvements
* Remove hard-coding of sponsor logos from `src/frontend/src/assets`, use DB stored logo URLs instead.
* Remove need for server restart on leaderboard definition addition / update
* Remove need for server restart on decision to use user password checks (move it from the env variable to a DB stored flag)
* Add an endpoint on the "/" that will list running leaderboards and link to them, instead of just throwing 404.
* Admin UI
* Smarter user authentication - SSO or something
* Display indication that the competition is over
* Display before-and-after scores when the competition ends
* Display leaderboard-around-user for large competitions

---

Made with 🐶 by [DAGsHub](https://dagshub.com/).
