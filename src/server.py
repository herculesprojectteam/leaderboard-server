import functools
import os

from flask import Flask, send_from_directory, redirect
from flask_restful import Resource, Api
from flask_restful.utils.cors import crossdomain
from leaderboard.competition_ranking_leaderboard import CompetitionRankingLeaderboard
from redis import ConnectionPool, Redis

from .conf import get_leaderboard_confs, check_allowed, check_end_competition, check_show_secret
from .leaderboard_api import LeaderboardAPI, always_true
from .sponsor_server import SponsorServer


def the_app(debug=False):
    static_folder = "frontend/build"
    app = Flask(__name__, static_folder=static_folder, static_url_path="/")
    api = Api(app)
    # This ensures that http://host/path goes to the same place as http://host/path/
    app.url_map.strict_slashes = False

    root_redirect_url = os.getenv('ROOT_REDIRECT_URL',
                                  'https://dagshub.com/Datahack/DataHack-Resources-2019/src/master/Leaderboard.md')
    if root_redirect_url:
        @app.route('/')
        def root():
            return redirect(root_redirect_url)

    redis_pass = os.getenv('REDIS_PASSWORD', None)
    if redis_pass is not None:
        redis_pass = f":{redis_pass}@"
    redis_url = f"redis://{redis_pass or ''}{os.getenv('REDIS_HOST', 'localhost')}:{os.getenv('REDIS_PORT', 6379)}"
    if debug:
        print(f"Redis = {redis_url}")
    connection_pool = ConnectionPool.from_url(redis_url)
    redis = Redis(connection_pool=connection_pool)

    leaderboards = get_leaderboard_confs(redis)
    print("Leaderboards:")
    print(leaderboards)

    def check_pass_func_generator(name, leaderboard):
        should_check_allowed = leaderboard.get(b'should_check_allowed')
        print(f'Leaderbaord {name} checking allowed: {should_check_allowed}')
        return functools.partial(check_allowed, redis, name) if should_check_allowed else always_true

    should_log = os.getenv("SHOULD_LOG", "false") == "true"
    print(f'Logging: {should_log}')

    leaderboard_apis = {
        name: LeaderboardAPI(
            CompetitionRankingLeaderboard(name, connection_pool=connection_pool),
            CompetitionRankingLeaderboard(f'{name}_SECRET', connection_pool=connection_pool),
            functools.partial(check_end_competition, redis, name),
            functools.partial(check_show_secret, redis, name),
            passwd_check=check_pass_func_generator(name, leaderboard),
            sponsor_server_url=leaderboard[b'sponsor_server_url'].decode(),
            debug=debug,
            log=f"log/{name}.csv" if should_log else None
        )
        for name, leaderboard in leaderboards.items()
    }

    def leaderboard_root():
        return send_from_directory(static_folder, "index.html")

    for name in leaderboards.keys():
        app.add_url_rule(f'/{name}/', view_func=leaderboard_root)

    class LeaderboardResource(Resource):

        @crossdomain('*')
        def get(self, leaderboard_name):
            return leaderboard_apis[leaderboard_name].get()

        @crossdomain('*')
        def post(self, leaderboard_name):
            return leaderboard_apis[leaderboard_name].post()

    api.add_resource(LeaderboardResource, f"/<string:leaderboard_name>/api")

    # For testing against a mock sponsor server
    api.add_resource(SponsorServer, '/mock_sponsor_server')

    return app


def main():
    debug = os.getenv('DEBUG', False)
    port = os.getenv('PORT', 5000)
    if debug:
        print('DEBUG MODE!')
    app = the_app(debug)
    app.run(debug=debug, port=port)
    return app


if __name__ == '__main__':
    main()
