import math
import random

rand = random.Random()

from flask import Flask, request
from flask_restful import Resource, Api
from flask_restful.utils.cors import crossdomain


class SponsorServer(Resource):

    @crossdomain('*')
    def post(self):
        # This is the same object submitted by the user in their "predictions" field.
        # See scripts/submit.py
        predictions = request.json
        print(predictions)
        score = len(predictions)
        score_obj = {'public_score': score * math.e * rand.uniform(0, 1),
                     'secret_score': (score % 5) * math.e * rand.uniform(0, 1)}
        print(score_obj)
        return score_obj

if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(SponsorServer, '/')

    app.run(port=8001, debug=True)