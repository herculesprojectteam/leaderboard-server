import hashlib
import random

from redis import Redis, ConnectionPool
import argparse

r = random.Random()


def genpasswd():
    random_bits = str(r.random()).encode()
    return hashlib.sha1(random_bits).hexdigest()


def get_leaderboard_confs(redis: Redis):
    leaderboard_names = [name.decode() for name in redis.smembers('leaderboard_names')]
    leaderboards = {name: redis.hgetall(f'LEADERBOARD_{name}') for name in leaderboard_names}
    return leaderboards


def add_leaderboard_conf(redis: Redis, conf):
    name = conf['name']
    redis.sadd('leaderboard_names', name)
    redis.hmset(f'LEADERBOARD_{name}', conf)


def set_user(redis: Redis, name: str):
    passwd = genpasswd()
    redis.hset(f'USER_{name}', 'passwd', passwd)
    return passwd


def get_user(redis: Redis, name: str):
    return redis.hgetall(f'USER_{name}')


def check_password(redis: Redis, name: str, passwd: str):
    stored_pass = redis.hget(f'USER_{name}', 'passwd')
    if stored_pass:
        return passwd == stored_pass.decode()
    else:
        return False


def check_allowed(redis: Redis, leaderboard_name: str, name: str, passwd: str):
    return redis.sismember(f'{leaderboard_name}_ALLOWED', name)

def end_competition(redis: Redis, name: str):
    redis.set(f'COMPETITION_END_{name}', True)
    print(f'Competition {name} ended!')


def unend_competition(redis: Redis, name: str):
    redis.delete(f'COMPETITION_END_{name}')
    print(f'Competition {name} resumed!')


def check_end_competition(redis: Redis, name: str):
    return redis.get(f'COMPETITION_END_{name}')


def show_secret(redis: Redis, name: str):
    redis.set(f'SHOW_SECRET_{name}', True)
    print(f'Showing secret results for {name}!')


def unshow_secret(redis: Redis, name: str):
    redis.delete(f'SHOW_SECRET_{name}')
    print(f'Not showing secret results for {name}!')


def check_show_secret(redis: Redis, name: str):
    return redis.get(f'SHOW_SECRET_{name}')

def get_results(redis: Redis, leaderboard_name: str):
    return redis.zrevrange(leaderboard_name, 0, -1)


def get_secret_results(redis: Redis, leaderboard_name: str):
    return get_results(redis, f'{leaderboard_name}_SECRET')

# If you want to manually run functions in this file instead of using the predefined commands, run the following from the project
# root folder:
# python
# >>> from redis import Redis
# >>> from redis.connection import ConnectionPool
# >>> redis = Redis(ConnectionPool.from_url('redis://localhost:6379')
# >>> from src.conf import *
# >>> get_leaderboard_confs(redis)
if __name__ == '__main__':
    parser = argparse.ArgumentParser("Show existing leaderboard confs or add a new one")

    parser.add_argument('--redis', '-r', default='http://localhost:6379')
    parser.add_argument('--add', '-a', action="store_true",
                        help="Add a new leaderboard configuration. "
                             "Set the name, display name and sponsor server URL "
                             "with -n, -d, -s")
    parser.add_argument('--user', '-u', action="store_true",
                        help="Add a new user. Set the name with -n. "
                             "Returns the new user's generated password, "
                             "to be used during submission.")
    parser.add_argument('--get_user', action="store_true",
                        help="Returns a stored user's details."
                             "Useful if the user forgets their password.")
    parser.add_argument('--end', action="store_true",
                        help="Mark a competition as ended."
                             "Will cause the server to stop accepting submissions, and display the secret leaderboard.")
    parser.add_argument('--unend', action="store_true",
                        help="Revert the --end flag")

    parser.add_argument('--name', '-n', type=str)
    parser.add_argument('--display_name', '-d', type=str)
    parser.add_argument('--sponsor_server_url', '-s', type=str)

    args = parser.parse_args()
    connection_pool = ConnectionPool.from_url(args.redis)
    redis = Redis(connection_pool=connection_pool)

    if args.add:
        add_leaderboard_conf(redis, {'name': args.name,
                                     'display_name': args.display_name,
                                     'sponsor_server_url': args.sponsor_server_url})
    elif args.user:
        print(f'Creating user: {args.name}')
        print(set_user(redis, args.name))
    elif args.get_user:
        print(get_user(redis, args.name))
    elif args.end:
        end_competition(redis, args.name)
    elif args.unend:
        unend_competition(redis, args.name)
    else:
        print(get_leaderboard_confs(redis))
