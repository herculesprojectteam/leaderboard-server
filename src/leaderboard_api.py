import json
import urllib.request
from datetime import datetime
import re

forbidden_log_chars = r'[,\n]'

from flask import request, abort
from flask_restful import Resource, reqparse
from flask_restful.utils.cors import crossdomain

submission_parser = reqparse.RequestParser()
submission_parser.add_argument('submitter', type=str, required=True)
submission_parser.add_argument('passwd', type=str, required=False)
submission_parser.add_argument('predictions', required=True)


def always_true(*args):
    return True


class LeaderboardAPI(Resource):

    def __init__(self, leaderboard, secret_leaderboard, competition_end_check, show_secret_check,
                 passwd_check=always_true, display_name="Leaderboard",
                 sponsor_server_url="http://localhost:8001/", debug=False, log=None):
        super(LeaderboardAPI, self).__init__()
        self.competition_end_check = competition_end_check
        self.show_secret_check = show_secret_check
        self.passwd_check = passwd_check
        self.debug = debug
        self.leaderboard = leaderboard
        self.secret_leaderboard = secret_leaderboard
        self.sponsor_server_url = sponsor_server_url
        self.display_name = display_name
        self.logfile = None
        if log:
            self.logfile = open(log, 'a', 1024)

    @crossdomain('*')
    def get(self):
        target_leaderboard = self.leaderboard
        secret = False
        if self.show_secret_check():
            secret = True
            target_leaderboard = self.secret_leaderboard
        if self.debug:
            print(f'Getting leaders from { target_leaderboard.leaderboard_name}')
        leaders = target_leaderboard.all_leaders()
        for i in range(len(leaders)):
            member_name_bytes = leaders[i]['member']
            leaders[i]['member'] = str(member_name_bytes, encoding='utf-8')
        if self.debug:
            print(leaders)

        return {'leaders': leaders, 'display_name': self.display_name, 'secret': secret}

    @crossdomain('*')
    def post(self):
        if self.competition_end_check():
            print('Submission received after competition end:')
            print(request.json)
            abort(403)
            return

        args = submission_parser.parse_args()
        if self.debug:
            print(args)

        print(f"Submission to {self.leaderboard.leaderboard_name} from: {args.submitter}")
        submitter = args.submitter
        passwd = args.passwd
        if not self.passwd_check(submitter, passwd):
            abort(403)
            return
        scores = self.submit_to_sponsor_server(request.json['predictions'])

        public_score = scores['public_score']
        secret_score = scores['secret_score']

        print(f"{args.submitter} Scores: {scores}")
        if self.logfile:
            self.logfile.write(f"{datetime.utcnow().timestamp()},{re.sub(forbidden_log_chars, '_', args.submitter[:100])},{public_score},{secret_score}\n")

        self.leaderboard.rank_member(submitter, public_score)
        self.secret_leaderboard.rank_member(submitter, secret_score)
        score_and_rank = self.leaderboard.score_and_rank_for(submitter)
        return score_and_rank

    def submit_to_sponsor_server(self, data):
        req = urllib.request.Request(self.sponsor_server_url,
                                     headers={'Content-Type': 'application/json'},
                                     data=json.dumps(data).encode())
        resp = urllib.request.urlopen(req)
        score = json.load(resp)
        return score
