import React from 'react';
import './style.sass';

const tryRequire = (path) => {
    try {
        require(`${path}`);
        return true
    } catch (err) {
        return false;
    }
};

class NavbarContent extends React.Component {
    render() {
        const sponsorFolder = tryRequire('./assets' + this.props.sponsorName + '/rightLogo.svg') ? this.props.sponsorName : "/default";

        return (
            <nav id="navbar" className="navbar navbar-dark sticky-top">
                <div className="datahack">
                    <a href="https://www.datahack.org.il/">
                        <img className="logo-lg" src={require('./assets/host/leftLogo.svg')} height="50" alt=""/>
                        <img className="logo-sm" src={require('./assets/host/leftLogoSm.svg')} height="50" alt=""/>
                    </a>
                </div>
                <div className="sponsor">
                    <a className="float-right" href="https://dagshub.com">
                        <img className="logo-lg" src={require('./assets' + sponsorFolder + '/rightLogo.svg')} height="50" alt=""/>
                        <img className="logo-sm" src={require('./assets' + sponsorFolder + '/rightLogoSm.svg')} height="50" alt=""/>
                    </a>
                </div>
            </nav>
        )
    }
}

export default NavbarContent;