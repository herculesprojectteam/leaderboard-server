import React from 'react';
import ReactDOM from 'react-dom';
import NavbarContent from './NavbarContent';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NavbarContent />, div);
  ReactDOM.unmountComponentAtNode(div);
});
