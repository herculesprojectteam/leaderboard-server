
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import './style.sass';
import Leaderboard from './Leaderboard';
import NavbarContent from "./NavbarContent";
import * as serviceWorker from './serviceWorker';

const currentPath = document.location.pathname.replace(/\/$/, ""); // Strip trailing slash
const currentURL = document.location.toString().replace(/\/$/, ""); // Strip trailing slash
const apiURL = `${currentURL}/api`;

ReactDOM.render(<NavbarContent sponsorName={currentPath} />, document.getElementById('leadernav'));
ReactDOM.render(<Leaderboard apiURL={apiURL} sponsorName={currentPath} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
