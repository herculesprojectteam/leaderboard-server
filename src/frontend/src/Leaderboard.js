import React from 'react';
import './style.sass';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import Fireworks from './Fireworks.js';

class Leaderboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            secret: false
        };
        this._clickScore = this._clickScore.bind(this);
        this._clickName = this._clickName.bind(this);
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData() {
        const fetchInit = {
            method: 'GET',
            mode: 'cors'
        };
        fetch(this.props.apiURL, fetchInit)
        .then(response => response.json())
        .then(data => {
            this.setState({
                list: data.leaders,
                secret: data.secret
            });
        })
        .catch(err => console.log('fetch error : ', err)); // TODO: Display error
    }

    componentDidMount() {
        if (this.props.apiURL === "mock") {
            let data = require('./mockData.json');
            this.setState({
                list: data
            });
        } else {
            this.timerID = setInterval(this.fetchData, 5000);
            this.fetchData();
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    _clickName() {
        let sorted = this.state.list.sort((a, b) => ("" + a.member).localeCompare(b.member));
        this.setState(sorted);
    }

    _clickScore() {
        let sorted = this.state.list.sort((a, b) => b.score - a.score);
        this.setState(sorted);
    }

    render() {
        // TODO: Loading spinner?
        let userlist = this.state.list.map((user, i) => <User key={i} member={user.member} rank={user.rank}
                                                           score={user.score} secret={this.state.secret}/>);

        return (
            <div>
                <Container>
                    <LeaderboardHeader sponsor={this.props.sponsorName}/>
                    <ColumnHeader onClickName={this._clickName}
                                  onClickScore={this._clickScore}
                    />
                    <div className="leaderboard-content">
                        {userlist}
                    </div>
                </Container>
                {this.state.secret && <Fireworks />}
            </div>
        )
    }

}

const LeaderboardHeader = (args) => {
    let sponsorName = args.sponsor ? args.sponsor.substring(1) : "DAGsHub";
    return (
        <div className="leadheader">
                <h2 className="leadeheader-h2">
                    <a href="/"><i className="fa fa-question-circle leaderboard-instructions"></i></a>
                    <div className="leaderboard-header-text"><span className="sponsor-name" >{sponsorName}</span> Leaderboard</div>
                    <div className="leaderboard-header-pad"></div>
                </h2>
        </div>
    )
};

const ColumnHeader = ({
                          onClickName,
                          onClickScore,
                      }) => (
    <div className="row colheader align-text-bottom">
        <div className="col-3">
            <h4>#</h4>
        </div>
        <div className="col-5 member">
            <h4 onClick={onClickName}>Name</h4>
        </div>
        <div className="col-4 score">
            <h4 onClick={onClickScore}>Score</h4>
        </div>
    </div>
);

ColumnHeader.propTypes = {
    onClick: PropTypes.func,
    onClickAll: PropTypes.func
};

const User = ({member, rank, score, secret}) => {
    let rowColor;
    switch (rank) {
        case 1:
            rowColor = "goldColor";
            break;
        case 2:
            rowColor = "silverColor";
            break;
        case 3:
            rowColor = "bronzeColor";
            break;
        default:
            rowColor = "regColor";
            break;
    }

    let rowClass = "users " + (secret ? rowColor : "");
    return (
        <Row className={rowClass}>
            <Col xs="3" className="rank">
                <h4 className="text-truncate">{rank}</h4>
            </Col>
            <Col xs="5" className="name text-truncate">
                {member}
            </Col>
            <Col xs="4">
                <h4 className="text-truncate">{score}</h4>
            </Col>
        </Row>
    )
};

User.propTypes = {
    member: PropTypes.string.isRequired,
    rank: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired
};

export default Leaderboard;