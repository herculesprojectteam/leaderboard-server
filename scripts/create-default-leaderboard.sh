#!/usr/bin/env bash
# Use this for easy local testing.
# Requires the redis instance to be running first.
sh -c "cd .. && python -m src.conf -a -n default -d Default -s http://localhost:8001/"