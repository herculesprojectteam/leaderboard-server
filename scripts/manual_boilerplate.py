from redis import Redis
from redis.connection import ConnectionPool
import os

red = Redis(connection_pool=ConnectionPool.from_url(os.getenv('REDIS_URL', 'redis://127.0.0.1:6379')))
from src.conf import *
